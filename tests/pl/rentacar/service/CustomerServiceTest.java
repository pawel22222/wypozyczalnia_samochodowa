package pl.rentacar.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerServiceTest {

    CustomerService customerService ;
    EmployeeService employeeService;

    @org.junit.Before
    public void setUp() throws Exception {
        customerService= new CustomerService();
        CarService.getInstance();
        employeeService = new EmployeeService();
        EmployeeService.getInstance();

    }

    @org.junit.Test
    public void getAll() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            customerService.employeeService = employeeService;

            List<Customer> customer = customerService.getAll();
            assertTrue(customer.size() >=1);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void get() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            customerService.employeeService = employeeService;

            Customer customer = customerService.get(4);
            assertTrue(customer.getEmail() != null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void insertGetDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            customerService.employeeService = employeeService;

            Customer customer = new Customer(1,"TestoweImie", "TestoweNazwisko", "111111111","tak@gmail.com","asasasas");
            customerService.insert(customer);

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM customer WHERE name = 'TestoweImie' AND surname = 'TestoweNazwisko' AND phone ='111111111' LIMIT 1");
            assertTrue(result.next());

            customerService.delete(result.getInt("customer_id"));
            Customer customer2 = customerService.get(result.getInt("customer_id"));

            assertTrue(customer2 == null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void editInsertDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            customerService.employeeService = employeeService;

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO customer(customer_id, name, surname, phone, email, address, rental_id) " +
                    "VALUES (10000, 'tak', 'tak', '999999999', 'nie@gmail.com', 'dedededed', 1)");
            Customer customer = new Customer(10000,"niii","niii","000000000", "nii@gmail.com", "niiii");
            customerService.edit(customer, 10000);
            Customer customer2 = customerService.get(10000);
            assertTrue(customer2.getName().equals("niii"));
            customerService.delete(10000);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

}