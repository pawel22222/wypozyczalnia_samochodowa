package pl.rentacar.service;

import org.junit.Before;
import pl.rentacar.model.Car;
import pl.rentacar.model.Employee;
import pl.rentacar.model.Rental;
import pl.rentacar.tools.Db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;

public class RentalServiceTest {

    RentalService rentalService ;
    EmployeeService employeeService;

    @org.junit.Before
    public void setUp() throws Exception {
        rentalService = new RentalService();
        employeeService = new EmployeeService();

    }


    @org.junit.Test
    public void getAll() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","superadmin");
            employeeService.setLoggedInUser(employee);
            rentalService.employeeService = employeeService;

            List<Rental> rentals = rentalService.getAll();
            assertTrue(rentals.size() >=1);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void get() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","superadmin");
            employeeService.setLoggedInUser(employee);
            rentalService.employeeService = employeeService;

            Rental rental = rentalService.get(1);
            assertTrue(rental.getName() != null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void insertGetDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","superadmin");
            employeeService.setLoggedInUser(employee);
            rentalService.employeeService = employeeService;

            Rental rental = new Rental(1, "test", "test","test");
            rentalService.insert(rental);

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM rental WHERE name = 'test'  LIMIT 1");
            assertTrue(result.next());

            rentalService.delete(result.getInt("rental_id"));
            Rental rental2 = rentalService.get(result.getInt("rental_id"));

            assertTrue(rental2 == null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void editInsertDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","superadmin");
            employeeService.setLoggedInUser(employee);
            rentalService.employeeService = employeeService;

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO rental (rental_id, name, city, address)  VALUES (9999, 'tak', 'tak', 'tak')");
            Rental rental = new Rental("nowy", "nowy", "nowy");
            rentalService.edit(rental, 9999);
            Rental rental2 = rentalService.get(9999);
            assertTrue(rental2.getCity().equals("nowy"));
            rentalService.delete(9999);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }
}