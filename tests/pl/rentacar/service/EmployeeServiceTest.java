package pl.rentacar.service;

import org.junit.Before;
import pl.rentacar.model.Car;
import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeServiceTest {

    EmployeeService employeeService;

    @Before
    public void setUp() throws Exception {
        employeeService = new EmployeeService();
        EmployeeService.getInstance();
    }


    @org.junit.Test
    public void getAll() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);

            List<Employee> employee2 = employeeService.getAll();
            assertTrue(employee2.size() >= 1);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void get() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);

            Employee employee2 = employeeService.get(1);
            assertTrue(employee2.getName() != null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void insertGetDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);

            Employee employee2 = new Employee(1,1,"Test", "Test","Test","Test","admin");
            employeeService.insert(employee2);

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM employee WHERE rental_id = 1 AND name = 'Test' AND surname ='Test' AND Login = 'Test' LIMIT 1");
            assertTrue(result.next());

            employeeService.delete(result.getInt("employee_id"));
            Employee employee3 = employeeService.get(result.getInt("employee_id"));

            assertTrue(employee3 == null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }


}