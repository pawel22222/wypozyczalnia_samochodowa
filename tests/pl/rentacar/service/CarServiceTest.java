package pl.rentacar.service;

import pl.rentacar.model.Car;
import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;

public class CarServiceTest {

    CarService carService ;
    EmployeeService employeeService;

    @org.junit.Before
    public void setUp() throws Exception {
        carService = new CarService();
        CarService.getInstance();
        employeeService = new EmployeeService();
        EmployeeService.getInstance();

    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    @org.junit.Test
    public void getAll() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            carService.employeeService = employeeService;
            List<Car> cars = carService.getAll();
            assertTrue(cars.size() >=1);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void get() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            carService.employeeService = employeeService;
            Car car = carService.get(14);
            assertTrue(car.getMake() != null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void insertGetDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            carService.employeeService = employeeService;
            Car car = new Car(1,"Lexus", "nowyModel",2020,"elektryczny", 100, 999000.00, "dostępny od ręki",false);
            carService.insert(car);

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM car WHERE rental_id = 1 AND model = 'nowyModel' AND year =2020 AND fuel = 'elektryczny' LIMIT 1");
            assertTrue(result.next());

            carService.delete(result.getInt("rental_id"));
            Car car2 = carService.get(result.getInt("rental_id"));

            assertTrue(car2 == null);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void editInsertDeleteTest() {
        try {
            Employee employee = new Employee(1,1,"Testowy","Testowy","Testowy","Testowy","admin");
            employeeService.setLoggedInUser(employee);
            carService.employeeService = employeeService;
            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO car (car_id, rental_id, make, model, year, fuel, mileage, price, availability) " +
                    "VALUES (10000, 1, 'Lexus', 'nowyModel', 2020, 'elektryczny', 100, 100, 'dostępny od ręki')");
            Car car = new Car(1,"tak","tak",2021, "tak",1,1.00, "dostępny od ręki",false);
            carService.edit(car, 10000);
            Car car2 = carService.get(10000);
            assertTrue(car2.getMake().equals("tak"));
            carService.delete(10000);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

}