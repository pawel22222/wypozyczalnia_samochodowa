package pl.rentacar.service;

import org.junit.Before;
import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.model.Employee;
import pl.rentacar.model.Rent;
import pl.rentacar.tools.Db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class RentServiceTest {

    RentService rentService ;
    EmployeeService employeeService;

    @org.junit.Before
    public void setUp() throws Exception {
        rentService = new RentService();
        employeeService = new EmployeeService();
    }


    @org.junit.Test
    public void insertTest() {
        try {
            LocalDate rentDate = LocalDate.of(2020, 1, 8);
            LocalDate returnDate = LocalDate.of(2021, 1, 8);
            Car car = new Car(1,"Lexus", "nowyModel",2020,"elektryczny", 100, 999000.00, "dostępny od ręki",false);
            Customer customer = new Customer(1,"TestoweImie", "TestoweNazwisko", "111111111","tak@gmail.com","asasasas");

            Rent rent = new Rent(1,2,rentDate,returnDate,4, car,customer);
            rentService.insert(rent);

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM rent WHERE car_id =2 AND customer_id=4  LIMIT 1");
            assertTrue(result.next());

            rentService.delete(result.getInt("rent_id"));

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }

    @org.junit.Test
    public void editInsertDeleteTest() {
        try {

            Connection connection = Db.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO rent (rent_id, rent_date, return_date, customer_id, car_id)  VALUES (10000, '2020-01-01', '2021-01-01',4 , 2)");

            LocalDate rentDate = LocalDate.of(2020, 1, 8);
            LocalDate returnDate = LocalDate.of(2021, 1, 8);
            Car car = new Car(1,"Lexus", "nowyModel",2020,"elektryczny", 100, 999000.00, "dostępny od ręki",false);
            Customer customer = new Customer(1,"TestoweImie", "TestoweNazwisko", "111111111","tak@gmail.com","asasasas");

            Rent rent = new Rent(1,2,rentDate,returnDate,4, car,customer);

            rentService.edit(rent, 10000);

            rentService.delete(10000);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
            fail();
        }
    }
}