package pl.rentacar.model;

import java.time.LocalDate;

public class Rent {
    private Integer id;
    private Integer carId;
    private LocalDate rentDate;
    private LocalDate returnDate;
    private Integer customerId;
    private Car car;
    private Customer customer;

    public Rent() {
    }

    public Rent(Integer id, Integer carId, LocalDate rentDate, LocalDate returnDate, Integer customerId, Car car, Customer customer) {
        this.id = id;
        this.carId = carId;
        this.rentDate = rentDate;
        this.returnDate = returnDate;
        this.customerId = customerId;
        this.car = car;
        this.customer = customer;
    }

    public Rent(Integer carId, LocalDate rentDate, LocalDate returnDate, Integer customerId, Car car, Customer customer) {
        this.carId = carId;
        this.rentDate = rentDate;
        this.returnDate = returnDate;
        this.customerId = customerId;
        this.car = car;
        this.customer = customer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public LocalDate getRentDate() {
        return rentDate;
    }

    public void setRentDate(LocalDate rentDate) {
        this.rentDate = rentDate;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return returnDate + " : " + customer.getName() + " " + customer.getSurname() + ", " +
                customer.getPhone() + " <-> " + car.getMake() + " " +
                car.getModel() + (car.getYear() != 0 ? car.getYear() : "");
    }
}
