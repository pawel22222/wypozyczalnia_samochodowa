package pl.rentacar.model;

public class Employee {
    private Integer id;
    private Integer rentalId;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String role;

    public Employee() {
    }

    public Employee(final Integer id, final Integer rentalId, final String name, final String surname,
                    final String login, final String password, final String role) {
        this.id = id;
        this.rentalId = rentalId;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public Employee(final Integer rentalId, final String name, final String surname, final String login,
                    final String password, final String role) {
        this.id = null;
        this.rentalId = rentalId;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getRentalId() {
        return rentalId;
    }

    public void setRentalId(final Integer rentalId) {
        this.rentalId = rentalId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isSuperAdmin() {
        return "superadmin".equals(role);
    }

    public boolean isAdmin() {
        return "admin".equals(role);
    }

    public boolean isUser() {
        return "user".equals(role);
    }

    @Override
    public String toString() {
        return name + " " + surname + " | login: " + login + " | " + getRole();
    }

    public String getRole() {
        return role;
    }
}
