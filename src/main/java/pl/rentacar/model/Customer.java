package pl.rentacar.model;

public class Customer {
    private Integer id;
    private String name;
    private String surname;
    private String phone;
    private String email;
    private String address;

    public Customer() {
    }

    public Customer(final Integer id, final String name, final String surname, final String phone,
                    final String email, final String address) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    public Customer(final String name, final String surname, final String phone, final String email,
                    final String address) {
        this.id = null;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(final String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name + " " + surname + " | tel. " + phone + " | " + email + " | " + address;
    }
}
