package pl.rentacar.model;

public class Rental {
    private Integer id;
    private String name;
    private String address;
    private String city;

    public Rental() {
    }

    public Rental(final Integer id, final String name, final String address, final String city) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public Rental(final String name, final String address, final String city) {
        this.id = null;
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return name + " | " + city + " | " + address;
    }
}
