package pl.rentacar.model;

public class Car {
    private Integer id;
    private Integer rentalId;
    private String make;
    private String model;
    private Integer year;
    private String fuel;
    private Integer mileage;
    private Double price;
    private String availability;
    private Boolean borrowed;

    public String toString() {
        return make + " " + model + " | " + (year != 0 ? year + " | " : "") +
                price + " PLN | " + (borrowed ? "wypozyczony" : "dostepny");
    }

    public Car() {
    }

    public Car(Integer rentalId, String make, String model, int year, String fuel, int mileage, Double price, String availability, Boolean borrowed) {
        this.rentalId = rentalId;
        this.make = make;
        this.model = model;
        this.year = year;
        this.fuel = fuel;
        this.mileage = mileage;
        this.price = price;
        this.availability = availability;
        this.borrowed = borrowed;
    }

    public Car(Integer id, Integer rentalId, String make, String model, int year, String fuel, int mileage, Double price, String availability, Boolean borrowed) {
        this.id = id;
        this.rentalId = rentalId;
        this.make = make;
        this.model = model;
        this.year = year;
        this.fuel = fuel;
        this.mileage = mileage;
        this.price = price;
        this.availability = availability;
        this.borrowed = borrowed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRentalId() {
        return rentalId;
    }

    public void setRentalId(Integer rentalId) {
        this.rentalId = rentalId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public Boolean getBorrowed() {
        return borrowed;
    }

    public void setBorrowed(Boolean borrowed) {
        this.borrowed = borrowed;
    }
}
