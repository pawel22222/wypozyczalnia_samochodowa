package pl.rentacar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class App extends Application {
    public static Stage stage;

    @Override
    public void start(Stage primaryStage) {
        // wczytanie szkieletu
        FXMLLoader loader = new FXMLLoader();

        Scene scene = null;
        try {
            URL resource = App.class.getClassLoader().getResource("auth/login.fxml");
            loader.setLocation(resource);
            Parent parent = FXMLLoader.load(Objects.requireNonNull(resource));
            parent.requestFocus();
            scene = new Scene(parent);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
            System.out.println("Nie można załadować struktury aplikacji!");

            System.exit(1);
        }

        primaryStage.setScene(scene);
        primaryStage.setTitle("Rent a Car");
        primaryStage.setResizable(false);
        primaryStage.setMinWidth(640 + 15);
        primaryStage.setMinHeight(500 + 32);
        primaryStage.setMaxWidth(640 + 15);
        primaryStage.setMaxHeight(500 + 32);
        primaryStage.show();

        stage = primaryStage;

        initializeCloseConfirmationDialog(primaryStage);
    }

    // wysłanie wiadomości końcowej do serwera i wyświetlenie dialogu zamykania
    protected void initializeCloseConfirmationDialog(final Stage primaryStage) {
        primaryStage.setOnCloseRequest(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Zamykanie programu");
            alert.setHeaderText("Zamykanie programu");
            alert.setContentText("Czy na pewno chcesz zamknąć program?");
            ButtonType yesButton = new ButtonType("Tak", ButtonBar.ButtonData.YES);
            ButtonType cancelButton = new ButtonType("Nie", ButtonBar.ButtonData.NO);
            alert.getButtonTypes().setAll(yesButton, cancelButton);
            alert.showAndWait().ifPresent(type -> {
                if (type == yesButton) {
                    alert.close();
                    System.exit(0);
                } else if (type == cancelButton) {
                    event.consume();
                }
            });
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
