package pl.rentacar.tools;

import pl.rentacar.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Db {

    private static Connection databaseLink;

   /* public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://" + Config.DB_HOST
                + "/" + Config.DB_NAME
                + "?user=" + Config.DB_USER
                + "&password=" + Config.DB_PASSWORD);
    }*/

    public static Connection getConnection(){
        String databaseName = "rentacar";
        String databaseUser = "root";
        String databasePassword = "";
        String url = "jdbc:mysql://localhost:3306/" + databaseName + "?autoReconnect=true&useSSL=false";

        try{
            Class.forName("com.mysql.jdbc.Driver");
            databaseLink = DriverManager.getConnection(url, databaseUser, databasePassword);

        }catch(Exception e){
            e.printStackTrace();
            e.getCause();
        }

        return databaseLink;
    }

}
