package pl.rentacar;

import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.model.Rent;
import pl.rentacar.model.Rental;

public class State {
    public static Customer currentCustomer = null;
    public static Rental currentRental = null;
    public static Car currentCar = null;
    public static Rent currentRent = null;
}
