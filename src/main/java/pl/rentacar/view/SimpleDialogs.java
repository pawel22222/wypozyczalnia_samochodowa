package pl.rentacar.view;

import javafx.application.Platform;
import javafx.scene.control.Alert;

public class SimpleDialogs {

    public static void warning(String str) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Ostrzeżenie");
            alert.setHeaderText(null);
            alert.setContentText(str);

            alert.showAndWait();
        });
    }

    public static void info(String str) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informacja");
            alert.setHeaderText(null);
            alert.setContentText(str);

            alert.showAndWait();
        });
    }

    public static void error(String str) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText(null);
            alert.setContentText(str);

            alert.showAndWait();
        });
    }

    public static void notImplementedYet() {
        warning("Funkcja jeszcze nie została zaimplementowana");
    }
}
