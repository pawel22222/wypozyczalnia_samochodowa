package pl.rentacar.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.rentacar.App;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class ViewManager {
    private static ViewManager instance = null;

    public static ViewManager getInstance() {
        if (instance == null) {
            instance = new ViewManager();
        }
        return instance;
    }

    public void switchToView(String fxmlName) {
        final Stage stage = App.stage;

        try {
            FXMLLoader loader = new FXMLLoader();

            URL resource = App.class.getClassLoader().getResource(fxmlName);
            loader.setLocation(resource);
            Parent parent = FXMLLoader.load(Objects.requireNonNull(resource));
            parent.requestFocus();
            stage.setScene(new Scene(parent));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
