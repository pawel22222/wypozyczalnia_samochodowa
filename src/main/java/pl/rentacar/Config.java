package pl.rentacar;

public class Config {
    public static String DB_HOST = "localhost";
    public static String DB_NAME = "rentacar";
    public static String DB_USER = "root";
    public static String DB_PASSWORD = "secret";
}
