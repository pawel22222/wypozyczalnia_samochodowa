package pl.rentacar.service;

import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.model.Rent;
import pl.rentacar.tools.Db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RentService {
    private static RentService instance = null;

    private final EmployeeService employeeService;
    private final CarService carService;
    private final CustomerService customerService;
   // private final CustomerService customerService;

    public RentService() {
        employeeService = EmployeeService.getInstance();
        carService = CarService.getInstance();
        customerService = CustomerService.getInstance();
    }

    public static RentService getInstance() {
        if (instance == null) {
            instance = new RentService();
        }

        return instance;
    }

    public List<Rent> getAll() throws SQLException, ClassNotFoundException {
        List<Rent> rents = new ArrayList<>();

        final Connection connection = Db.getConnection();
        final Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("SELECT rent_id, car_id, customer_id, rent_date, return_date " +
                "from rent order by rent_date, customer_id, return_date");

        while (resultSet.next()) {
            final Rent rent = new Rent();
            rent.setId(resultSet.getInt("rent_id"));
            rent.setCarId(resultSet.getInt("car_id"));
            rent.setCustomerId(resultSet.getInt("customer_id"));
            rent.setRentDate(resultSet.getDate("rent_date").toLocalDate());
            rent.setReturnDate(resultSet.getDate("return_date").toLocalDate());

            Car car = carService.get(rent.getCarId());
            rent.setCar(car);

            Customer customer = customerService.get(rent.getCustomerId());
            rent.setCustomer(customer);

            rents.add(rent);
        }

        connection.close();

        return rents;
    }

    public void insert(Rent rent) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO rent (rent_date, return_date, customer_id, car_id)  VALUES (?, ?, ?, ?)");

        preparedStatement.setDate(1, Date.valueOf(rent.getRentDate()));
        preparedStatement.setDate(2, Date.valueOf(rent.getReturnDate()));
        preparedStatement.setInt(3, rent.getCustomerId());
        preparedStatement.setInt(4, rent.getCarId());

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void edit(Rent rent, Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("UPDATE rent SET rent_date=?, return_date=?, customer_id=?, car_id=? " +
                        "WHERE rent_id = ?");

        preparedStatement.setDate(1, Date.valueOf(rent.getRentDate()));
        preparedStatement.setDate(2, Date.valueOf(rent.getReturnDate()));
        preparedStatement.setInt(3, rent.getCustomerId());
        preparedStatement.setInt(4, rent.getCarId());
        preparedStatement.setInt(5, id);

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void delete(Integer id) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("DELETE FROM rent WHERE rent_id = ?");

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        connection.close();
    }
}
