package pl.rentacar.service;

import pl.rentacar.model.Car;
import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarService {
    private static CarService instance = null;
    public EmployeeService employeeService;

    public CarService() {
        employeeService = EmployeeService.getInstance();
    }

    public static CarService getInstance() {
        if (instance == null) {
            instance = new CarService();
        }

        return instance;
    }

    public List<Car> getAll() throws SQLException {
        List<Car> cars = new ArrayList<>();

        final Employee loggedInUser = employeeService.getLoggedInUser();

        final Connection connection = Db.getConnection();
        final Statement statement = connection.createStatement();

        ResultSet resultSet;
        if (loggedInUser.isAdmin()) {
            resultSet = statement.executeQuery("SELECT car_id, rental_id, make, model, year, fuel, mileage, price, availability, " +
                    "(SELECT count(*) from rent WHERE car.car_id = rent.car_id) as borrowed " +
                    "from car order by borrowed, make, model");
        } else {
            resultSet = statement.executeQuery("SELECT car_id, rental_id, make, model, year, fuel, mileage, price, availability, " +
                    "(SELECT count(*) from rent WHERE car.car_id = rent.car_id) as borrowed " +
                    "from car WHERE rental_id = " + loggedInUser.getRentalId() + " order by borrowed, make, model");
        }

        while (resultSet.next()) {
            final Car car = new Car();
            car.setId(resultSet.getInt("car_id"));
            car.setRentalId(resultSet.getInt("rental_id"));
            car.setMake(resultSet.getString("make"));
            car.setModel(resultSet.getString("model"));
            car.setYear(resultSet.getInt("year"));
            car.setFuel(resultSet.getString("fuel"));
            car.setMileage(resultSet.getInt("mileage"));
            car.setPrice(resultSet.getDouble("price"));
            car.setAvailability(resultSet.getString("availability"));
            car.setBorrowed(resultSet.getBoolean("borrowed"));

            cars.add(car);
        }

        connection.close();

        return cars;
    }

    public Car get(Integer id) throws SQLException {
        final Employee loggedInUser = employeeService.getLoggedInUser();

        final Connection connection = Db.getConnection();

        PreparedStatement preparedStatement;
        if (loggedInUser.isAdmin()) {
            preparedStatement = connection.prepareStatement("SELECT car_id, rental_id, make, model, year, fuel, mileage, price, availability, " +
                    "(SELECT count(*) from rent WHERE car.car_id = rent.car_id) as borrowed " +
                    "from car WHERE car_id = ? order by borrowed, make, model");
            preparedStatement.setInt(1, id);
        } else {
            preparedStatement = connection.prepareStatement("SELECT car_id, rental_id, make, model, year, fuel, mileage, price, availability, " +
                    "(SELECT count(*) from rent WHERE car.car_id = rent.car_id) as borrowed " +
                    "from car WHERE car_id = ? AND rental_id = ? order by borrowed, make, model");
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, loggedInUser.getRentalId());
        }

        final ResultSet resultSet = preparedStatement.executeQuery();

        Car car = null;
        if (resultSet.next()) {
            car = new Car();
            car.setId(resultSet.getInt("car_id"));
            car.setRentalId(resultSet.getInt("rental_id"));
            car.setMake(resultSet.getString("make"));
            car.setModel(resultSet.getString("model"));
            car.setYear(resultSet.getInt("year"));
            car.setFuel(resultSet.getString("fuel"));
            car.setMileage(resultSet.getInt("mileage"));
            car.setPrice(resultSet.getDouble("price"));
            car.setAvailability(resultSet.getString("availability"));
            car.setBorrowed(resultSet.getBoolean("borrowed"));
        }

        connection.close();

        return car;
    }

    public void insert(Car car) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO car (rental_id, make, model, year, fuel, mileage, price, availability) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

        final Employee loggedInUser = employeeService.getLoggedInUser();

        preparedStatement.setInt(1, loggedInUser.getRentalId());
        preparedStatement.setString(2, car.getMake());
        preparedStatement.setString(3, car.getModel());
        preparedStatement.setInt(4, car.getYear());
        preparedStatement.setString(5, car.getFuel());
        preparedStatement.setInt(6, car.getMileage());
        preparedStatement.setDouble(7, car.getPrice());
        preparedStatement.setString(8, car.getAvailability());

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void edit(Car car, Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("UPDATE car SET rental_id=?, make=?, model=?, year=?, fuel=?, mileage=?, price=?, " +
                        "availability=? WHERE car_id = ?");

        final Employee loggedInUser = employeeService.getLoggedInUser();

        preparedStatement.setInt(1, loggedInUser.getRentalId());
        preparedStatement.setString(2, car.getMake());
        preparedStatement.setString(3, car.getModel());
        preparedStatement.setInt(4, car.getYear());
        preparedStatement.setString(5, car.getFuel());
        preparedStatement.setInt(6, car.getMileage());
        preparedStatement.setDouble(7, car.getPrice());
        preparedStatement.setString(8, car.getAvailability());
        preparedStatement.setInt(9, id);

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void delete(Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("DELETE FROM car WHERE car_id = ?");

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        connection.close();
    }
}
