package pl.rentacar.service;

import pl.rentacar.model.Customer;
import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerService {
    private Employee loggedInUser = null;

    private static CustomerService instance = null;
    public EmployeeService employeeService;

    public CustomerService() {
        employeeService = EmployeeService.getInstance();
    }

    public static CustomerService getInstance() {
        if (instance == null) {
            instance = new CustomerService();
        }

        return instance;
    }

    public Employee getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(final Employee loggedInUser) {
        this.loggedInUser = loggedInUser;
    }


    public List<Customer> getAll() throws SQLException {
        List<Customer> customers = new ArrayList<>();

        final Employee loggedInUser = employeeService.getLoggedInUser();

        final Connection connection = Db.getConnection();
        final Statement statement = connection.createStatement();

        ResultSet resultSet;
        if (loggedInUser.isAdmin()) {
            resultSet = statement.executeQuery("SELECT customer_id, name, surname, phone, email, address " +
                    "from customer order by surname, name, email");
        } else {
            resultSet = statement.executeQuery("SELECT customer_id, name, surname, phone, email, address " +
                    "from customer WHERE rental_id = " + loggedInUser.getRentalId() + " order by surname, name, email");
        }

        while (resultSet.next()) {
            final Customer customer = new Customer();
            customer.setId(resultSet.getInt("customer_id"));
            customer.setName(resultSet.getString("name"));
            customer.setSurname(resultSet.getString("surname"));
            customer.setPhone(resultSet.getString("phone"));
            customer.setEmail(resultSet.getString("email"));
            customer.setAddress(resultSet.getString("address"));

            customers.add(customer);
        }

        connection.close();

        return customers;
    }

    public Customer get(Integer id) throws SQLException, ClassNotFoundException {
        final Employee loggedInUser = employeeService.getLoggedInUser();

        final Connection connection = Db.getConnection();

        PreparedStatement preparedStatement;
        if (employeeService.isAdmin()) {
            preparedStatement = connection.prepareStatement("SELECT customer_id, name, surname, phone, email, address " +
                    "from customer where customer_id = ?");

            preparedStatement.setInt(1, id);
        } else {
            preparedStatement = connection.prepareStatement("SELECT customer_id, name, surname, phone, email, address " +
                    "from customer where customer_id = ? AND rental_id = ?");

            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, loggedInUser.getRentalId());
        }
        final ResultSet resultSet = preparedStatement.executeQuery();

        Customer customer = null;
        if (resultSet.next()) {
            customer = new Customer();
            customer.setId(resultSet.getInt("customer_id"));
            customer.setName(resultSet.getString("name"));
            customer.setSurname(resultSet.getString("surname"));
            customer.setPhone(resultSet.getString("phone"));
            customer.setEmail(resultSet.getString("email"));
            customer.setAddress(resultSet.getString("address"));
        }

        connection.close();

        return customer;
    }

    public void insert(Customer customer) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO customer " +
                        "(name, surname, phone, email, address, rental_id)  VALUES (?, ?, ?, ?, ?, ?)");

        final Employee loggedInUser = employeeService.getLoggedInUser();

        preparedStatement.setString(1, customer.getName());
        preparedStatement.setString(2, customer.getSurname());
        preparedStatement.setString(3, customer.getPhone());
        preparedStatement.setString(4, customer.getEmail());
        preparedStatement.setString(5, customer.getAddress());
        preparedStatement.setInt(6, loggedInUser.getRentalId());

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void edit(Customer customer, Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("UPDATE customer SET name=?, surname=?, phone=?, email=?, address=? " +
                        "WHERE customer_id = ?");

        preparedStatement.setString(1, customer.getName());
        preparedStatement.setString(2, customer.getSurname());
        preparedStatement.setString(3, customer.getPhone());
        preparedStatement.setString(4, customer.getEmail());
        preparedStatement.setString(5, customer.getAddress());
        preparedStatement.setInt(6, id);

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void delete(Integer id) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("DELETE FROM customer WHERE customer_id = ?");

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        connection.close();
    }
}
