package pl.rentacar.service;

import pl.rentacar.model.Rental;
import pl.rentacar.tools.Db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RentalService {
    private static RentalService instance = null;
    public EmployeeService employeeService;

    public RentalService() {
        employeeService = EmployeeService.getInstance();
    }

    public static RentalService getInstance() {
        if (instance == null) {
            instance = new RentalService();
        }

        return instance;
    }

    public List<Rental> getAll() throws SQLException, ClassNotFoundException {
        List<Rental> rentals = new ArrayList<>();

        final Connection connection = Db.getConnection();
        final Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery("SELECT rental_id, name, city, address " +
                "from rental order by name");

        while (resultSet.next()) {
            final Rental rental = new Rental();
            rental.setId(resultSet.getInt("rental_id"));
            rental.setName(resultSet.getString("name"));
            rental.setCity(resultSet.getString("city"));
            rental.setAddress(resultSet.getString("address"));

            rentals.add(rental);
        }

        connection.close();

        return rentals;
    }

    public Rental get(Integer id) throws SQLException {
        final Connection connection = Db.getConnection();

        if (!employeeService.getLoggedInUser().isSuperAdmin()) {
            return null;
        }

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT rental_id, name, city, address " +
                "from rental where rental_id = ?");

        preparedStatement.setInt(1, id);

        final ResultSet resultSet = preparedStatement.executeQuery();

        Rental rental = null;
        if (resultSet.next()) {
            rental = new Rental();
            rental.setId(resultSet.getInt("rental_id"));
            rental.setName(resultSet.getString("name"));
            rental.setCity(resultSet.getString("city"));
            rental.setAddress(resultSet.getString("address"));
        }

        connection.close();

        return rental;
    }

    public Rental getByName(String rentalName) throws SQLException {
        final Connection connection = Db.getConnection();

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT rental_id, name, city, address " +
                "from rental where name = ?");

        preparedStatement.setString(1, rentalName);

        final ResultSet resultSet = preparedStatement.executeQuery();

        Rental rental = null;
        if (resultSet.next()) {
            rental = new Rental();
            rental.setId(resultSet.getInt("rental_id"));
            rental.setName(resultSet.getString("name"));
            rental.setCity(resultSet.getString("city"));
            rental.setAddress(resultSet.getString("address"));
        }

        connection.close();

        return rental;
    }

    public void insert(Rental rental) throws SQLException, ClassNotFoundException {
        if (!employeeService.getLoggedInUser().isSuperAdmin()) {
            return;
        }

        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO rental (name, city, address)  VALUES (?, ?, ?)");

        preparedStatement.setString(1, rental.getName());
        preparedStatement.setString(2, rental.getCity());
        preparedStatement.setString(3, rental.getAddress());

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void edit(Rental rental, Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("UPDATE rental SET name=?, city=?, address=? " +
                        "WHERE rental_id = ?");

        preparedStatement.setString(1, rental.getName());
        preparedStatement.setString(2, rental.getCity());
        preparedStatement.setString(3, rental.getAddress());
        preparedStatement.setInt(4, id);

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void delete(Integer id) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("DELETE FROM rental WHERE rental_id = ?");

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        connection.close();
    }
}
