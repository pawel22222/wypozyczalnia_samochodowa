package pl.rentacar.service;

import pl.rentacar.model.Employee;
import pl.rentacar.tools.Db;
import pl.rentacar.tools.PasswordEncryptor;
import pl.rentacar.view.ViewManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeService {
    public Employee loggedInUser = null;

    private static EmployeeService instance = null;

    public static EmployeeService getInstance() {
        if (instance == null) {
            instance = new EmployeeService();
        }

        return instance;
    }

    public Employee getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(final Employee loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public boolean isThere(String login, String password) throws SQLException, ClassNotFoundException {
        final String encryptedPassword = PasswordEncryptor.encrypt(password);

        final Connection connection = Db.getConnection();

        final PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT count(*) as cnt FROM" +
                        " employee WHERE login = ? AND password = ?");
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, encryptedPassword);

        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            connection.close();
            return false;
        }

        int rowsCount = resultSet.getInt("cnt");

        connection.close();

        return rowsCount > 0;
    }

    public boolean isThere(String login) throws SQLException {
        final Connection connection = Db.getConnection();

        final PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT count(*) as cnt FROM" +
                        " employee WHERE login = ?");
        preparedStatement.setString(1, login);

        ResultSet resultSet = preparedStatement.executeQuery();

        if (!resultSet.next()) {
            connection.close();
            return false;
        }

        int rowsCount = resultSet.getInt("cnt");

        connection.close();

        return rowsCount > 0;
    }

    public boolean hasRole(String login, String role) throws SQLException {
        final Connection connection = Db.getConnection();

        final PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT count(*) as cnt FROM" +
                        " employee WHERE login = ? AND role like ?");
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, role);

        ResultSet resultSet = preparedStatement.executeQuery();

        if (!resultSet.next()) {
            connection.close();
            return false;
        }

        int rowsCount = resultSet.getInt("cnt");

        connection.close();

        return rowsCount > 0;
    }

    public boolean isAdmin(String login) throws SQLException {
        return hasRole(login, "admin");
    }

    public boolean isSuperAdmin(String login) throws SQLException {
        return hasRole(login, "superadmin");
    }

    public List<Employee> getAll() throws SQLException, ClassNotFoundException {
        List<Employee> employees = new ArrayList<>();

        final Connection connection = Db.getConnection();
        final Statement statement = connection.createStatement();

        ResultSet resultSet = statement
                .executeQuery("SELECT employee_id, rental_id, name, surname, login, password, role " +
                        "from employee order by surname, name, login");

        while (resultSet.next()) {
            final Employee employee = new Employee();
            employee.setId(resultSet.getInt("employee_id"));
            employee.setRentalId(resultSet.getInt("rental_id"));
            employee.setName(resultSet.getString("name"));
            employee.setSurname(resultSet.getString("surname"));
            employee.setLogin(resultSet.getString("login"));
            employee.setPassword(resultSet.getString("password"));
            employee.setRole(resultSet.getString("role"));

            employees.add(employee);
        }

        connection.close();

        return employees;
    }

    public Employee get(Integer id) throws SQLException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT employee_id, rental_id, name, surname, login, password, role " +
                        "from employee where employee_id = ?");

        preparedStatement.setInt(1, id);

        final ResultSet resultSet = preparedStatement.executeQuery();

        Employee employee = null;
        if (resultSet.next()) {
            employee = new Employee();
            employee.setId(resultSet.getInt("employee_id"));
            employee.setRentalId(resultSet.getInt("rental_id"));
            employee.setName(resultSet.getString("name"));
            employee.setSurname(resultSet.getString("surname"));
            employee.setLogin(resultSet.getString("login"));
            employee.setPassword(resultSet.getString("password"));
            employee.setRole(resultSet.getString("role"));
        }

        connection.close();

        return employee;
    }

    public Employee getByUsername(final String username) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT employee_id, rental_id, name, surname, login, password, role " +
                        "from employee where login = ?");

        preparedStatement.setString(1, username);
        final ResultSet resultSet = preparedStatement.executeQuery();

        Employee employee = null;
        if (resultSet.next()) {
            employee = new Employee();
            employee.setId(resultSet.getInt("employee_id"));
            employee.setRentalId(resultSet.getInt("rental_id"));
            employee.setName(resultSet.getString("name"));
            employee.setSurname(resultSet.getString("surname"));
            employee.setLogin(resultSet.getString("login"));
            employee.setPassword(resultSet.getString("password"));
            employee.setRole(resultSet.getString("role"));
        }

        connection.close();

        return employee;
    }

    public void insert(Employee employee) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO employee " +
                        "(rental_id, name, surname, login, password, role) " +
                        "VALUES (?, ?, ?, ?, ?, ?);");

        final String encryptedPassword = PasswordEncryptor.encrypt(employee.getPassword());

        preparedStatement.setInt(1, employee.getRentalId());
        preparedStatement.setString(2, employee.getName());
        preparedStatement.setString(3, employee.getSurname());
        preparedStatement.setString(4, employee.getLogin());
        preparedStatement.setString(5, encryptedPassword);
        preparedStatement.setString(6, employee.getRole());

        preparedStatement.executeUpdate();

        connection.close();
    }

    public void setAdmin(final Integer id, final boolean setAdmin) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("UPDATE employee SET role=? WHERE employee_id = ?");

        preparedStatement.setString(1, (setAdmin ? "admin" : "user"));
        preparedStatement.setInt(2, id);
        preparedStatement.executeUpdate();

        connection.close();
    }

    public void delete(Integer id) throws SQLException, ClassNotFoundException {
        final Connection connection = Db.getConnection();
        final PreparedStatement preparedStatement = connection
                .prepareStatement("DELETE FROM employee WHERE employee_id = ?");

        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();

        connection.close();
    }

    public boolean isLoggedIn() {
        return loggedInUser != null;
    }

    public boolean isAdmin() {
        if (!isLoggedIn()) return false;
        return loggedInUser.isAdmin();
    }

    public boolean isSuperAdmin() {
        if (!isLoggedIn()) return false;
        return loggedInUser.isSuperAdmin();
    }

    public void redirectByRole(ViewManager viewManager) {
        if (!isLoggedIn()) {
            viewManager.switchToView("auth/login.fxml");
        } else {
            if (isSuperAdmin()) {
                viewManager.switchToView("employee/superadmin.fxml");
            } else if (isAdmin()) {
                viewManager.switchToView("employee/admin.fxml");
            } else {
                viewManager.switchToView("employee/employee.fxml");
            }
        }
    }

    public void logout() {
        loggedInUser = null;
        redirectByRole(ViewManager.getInstance());
    }
}
