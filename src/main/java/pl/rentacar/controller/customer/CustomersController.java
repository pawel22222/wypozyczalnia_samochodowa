package pl.rentacar.controller.customer;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import pl.rentacar.State;
import pl.rentacar.model.Customer;
import pl.rentacar.service.CustomerService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class CustomersController implements Initializable {

    public ListView<Customer> customersList;
    private ViewManager viewManager;
    private CustomerService customerService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        customerService = CustomerService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        refreshCustomerList();
    }

    private void refreshCustomerList() {
        try {
            final List<Customer> customers = customerService.getAll();
            customersList.getItems().clear();
            customersList.getItems().addAll(customers);
        } catch (SQLException throwables) {
            SimpleDialogs.error("Błąd przy wczytywaniu listy klientów");
            throwables.printStackTrace();
        }
    }

    public void customersLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void customersBackClicked(ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }

    public void customersDeleteClicked(ActionEvent actionEvent) {
        final Customer selectedCustomer = customersList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCustomer == null) {
            SimpleDialogs.warning("Klient nie jest zaznaczony");
            return;
        }

        try {
            customerService.delete(selectedCustomer.getId());
            refreshCustomerList();
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu klienta");
            throwables.printStackTrace();
        }
    }

    public void customersAddClicked(ActionEvent actionEvent) {
        State.currentCustomer = null;
        viewManager.switchToView("customer/customerAddEdit.fxml");
    }

    public void customersEditClicked(ActionEvent actionEvent) {
        final Customer selectedCustomer = customersList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCustomer == null) {
            SimpleDialogs.warning("Klient nie jest zaznaczony");
            return;
        }

        State.currentCustomer = selectedCustomer;

        viewManager.switchToView("customer/customerAddEdit.fxml");
    }

    public void customerViewClicked(ActionEvent actionEvent) {
        final Customer selectedCustomer = customersList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCustomer == null) {
            SimpleDialogs.warning("Klient nie jest zaznaczony");
            return;
        }

        State.currentCustomer = selectedCustomer;

        viewManager.switchToView("customer/customerView.fxml");
    }
}
