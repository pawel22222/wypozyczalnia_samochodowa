package pl.rentacar.controller.customer;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.rentacar.State;
import pl.rentacar.model.Customer;
import pl.rentacar.service.CustomerService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class CustomerAddEditController implements Initializable {
    public TextField customerAddEditName;
    public TextField customerAddEditSurname;
    public TextField customerAddEditPhone;
    public TextField customerAddEditEmail;
    public TextArea customerAddEditAddress;

    private ViewManager viewManager;
    private CustomerService customerService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        customerService = CustomerService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        if (isEditMode()) {
            loadCustomerData(State.currentCustomer);
        }
    }

    private void loadCustomerData(Customer customer) {
        customerAddEditAddress.setText(customer.getAddress());
        customerAddEditEmail.setText(customer.getEmail());
        customerAddEditPhone.setText(customer.getPhone());
        customerAddEditName.setText(customer.getName());
        customerAddEditSurname.setText(customer.getSurname());
    }

    public void customerAddEditLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void customerAddEditBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("customer/customers.fxml");
    }

    public void customerAddEditSaveClicked(ActionEvent actionEvent) {
        Customer customer = new Customer();

        customer.setAddress(customerAddEditAddress.getText());
        customer.setEmail(customerAddEditEmail.getText());
        customer.setName(customerAddEditName.getText());
        customer.setSurname(customerAddEditSurname.getText());
        customer.setPhone(customerAddEditPhone.getText());

        if (isEditMode()) {
            try {
                customerService.edit(customer, State.currentCustomer.getId());
            } catch (SQLException throwables) {
                SimpleDialogs.error("Błąd przy edycji klienta");
                throwables.printStackTrace();
            }
        } else {
            try {
                customerService.insert(customer);
            } catch (SQLException | ClassNotFoundException throwables) {
                SimpleDialogs.error("Błąd przy dodawaniu klienta");
                throwables.printStackTrace();
            }
        }

        viewManager.switchToView("customer/customers.fxml");
    }

    private boolean isEditMode() {
        return State.currentCustomer != null;
    }
}
