package pl.rentacar.controller.customer;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import pl.rentacar.State;
import pl.rentacar.model.Customer;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.util.ResourceBundle;

public class CustomerViewController implements Initializable {

    public Label customerViewName;
    public Label customerViewSurname;
    public Label customerViewPhone;
    public Label customerViewEmail;
    public Label customerViewAddress;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        Customer c = State.currentCustomer;

        if (c == null) {
            SimpleDialogs.error("Nie wczytano klienta");
            return;
        }

        customerViewName.setText(c.getName());
        customerViewSurname.setText(c.getSurname());
        customerViewPhone.setText(c.getPhone());
        customerViewEmail.setText(c.getEmail());
        customerViewAddress.setText(c.getAddress());
    }

    public void customerViewLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void customerViewBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("customer/customers.fxml");
    }
}
