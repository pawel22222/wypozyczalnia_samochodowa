package pl.rentacar.controller.rent;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import pl.rentacar.State;
import pl.rentacar.model.Rent;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.util.ResourceBundle;

public class RentViewController implements Initializable {
    public Label rentViewCustomer;
    public Label rentViewCar;
    public Label rentViewRentDate;
    public Label rentViewReturnDate;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        Rent rent = State.currentRent;

        if (rent == null) {
            SimpleDialogs.error("Nie wczytano wypożyczenia");
            return;
        }

        rentViewCustomer.setText(rent.getCustomer().toString());
        rentViewCar.setText(rent.getCar().toString());
        rentViewRentDate.setText(rent.getRentDate().toString());
        rentViewReturnDate.setText(rent.getReturnDate().toString());
    }

    public void rentViewLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentViewBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("rent/rents.fxml");
    }
}
