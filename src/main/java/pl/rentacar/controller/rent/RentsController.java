package pl.rentacar.controller.rent;

import com.wypozycalnia.PDFgeneratorRentACar.PDF;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import pl.rentacar.State;
import pl.rentacar.model.Car;
import pl.rentacar.model.Rent;
import pl.rentacar.service.CarService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.service.RentService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import javax.swing.*;
//import javax.xml.ws.Service;
import java.awt.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class RentsController implements Initializable {
    public ListView<Rent> rentList;

    private ViewManager viewManager;
    private RentService rentService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        rentService = RentService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        refreshRentList();
    }

    private void refreshRentList() {
        try {
            final List<Rent> rents = rentService.getAll();
            rentList.getItems().clear();
            rentList.getItems().addAll(rents);
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy wczytywaniu listy wypożyczeń");
            throwables.printStackTrace();
        }
    }

    public void rentLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentBackClicked(ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }

    public void rentDeleteClicked(ActionEvent actionEvent) {
        final Rent selectedRent = getSelectedRent();
        if (selectedRent == null) return;

        try {
            rentService.delete(selectedRent.getId());
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu wypożyczenia");
            throwables.printStackTrace();
        }
        refreshRentList();
    }

    public void rentGeneratePDFClicked(ActionEvent actionEvent) {
        final Rent selectedRent = getSelectedRent();
        if (selectedRent == null) return;

        try {

            System.out.println(Desktop.isDesktopSupported() );
            String filePath = new JFileChooser().getFileSystemView().getDefaultDirectory().toString()+"\\Potwierdzenie_Rent_A_Car";
            Path path = Paths.get(filePath);
            Files.createDirectories(path);
            generatePDF(filePath+"\\rent_confirmation"+selectedRent.getId()+".pdf", selectedRent);



        } catch (Exception e) {
            SimpleDialogs.error("Błąd przy usuwaniu wypożyczenia");
            e.printStackTrace();
        }
        refreshRentList();
    }

    public void generatePDF(String filePath, Rent rent){

            try {

                   PDF potwierdzenie = new PDF();
                   potwierdzenie.addBlackLineOfTextToPDF(PDType1Font.TIMES_BOLD,16,170,750, "POTWIERDZENIE WYPOZYCZENIA");
                   potwierdzenie.addBlackLineOfTextToPDF(PDType1Font.TIMES_BOLD,16,200,720, "Id wypozyczenia w systemie: "+rent.getId());
                   potwierdzenie.addBlackLineOfTextToPDF(16,40,650, "Data wypozyczenia: "+rent.getRentDate());
                   potwierdzenie.addBlackLineOfTextToPDF(16,350,650, "Data zwrotu pojazdu: "+rent.getReturnDate());
                   potwierdzenie.addBlackLineOfTextToPDF(PDType1Font.TIMES_BOLD, 16,40,600, "Pojazd: ");
                   potwierdzenie.addBlackLineOfTextToPDF(16,40,570, ""+rent.getCar());
                   potwierdzenie.addBlackLineOfTextToPDF(PDType1Font.TIMES_BOLD, 16,40,520, "Klient: ");
                   potwierdzenie.addBlackLineOfTextToPDF(16,40,490, ""+rent.getCustomer());

                   potwierdzenie.saveAndCloseDocument(filePath);
                   Desktop.getDesktop().browse(new URL( "file:///"+filePath).toURI());

            }catch(Exception e) {
                e.printStackTrace();
                e.getCause();
            }
    }


    public void rentAddClicked(ActionEvent actionEvent) {
        State.currentRent = null;
        viewManager.switchToView("rent/rentAddEdit.fxml");
    }

    public void rentEditClicked(ActionEvent actionEvent) {
        final Rent selectedRent = getSelectedRent();
        if (selectedRent == null) return;

        State.currentRent = selectedRent;

        viewManager.switchToView("rent/rentAddEdit.fxml");
    }

    public void rentViewClicked(ActionEvent actionEvent) {
        final Rent selectedRent = getSelectedRent();
        if (selectedRent == null) return;

        State.currentRent = selectedRent;

        viewManager.switchToView("rent/rentView.fxml");
    }

    private Rent getSelectedRent() {
        final Rent selectedRent = rentList.getSelectionModel().getSelectedItems().get(0);

        if (selectedRent == null) {
            SimpleDialogs.warning("Klient nie jest zaznaczony");
            return null;
        }
        return selectedRent;
    }
}
