package pl.rentacar.controller.rent;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import pl.rentacar.State;
import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.model.Rent;
import pl.rentacar.service.CarService;
import pl.rentacar.service.CustomerService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.service.RentService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class RentAddEditController implements Initializable {
    public ComboBox<Customer> rentAddEditCustomer;
    public ComboBox<Car> rentAddEditCar;
    public DatePicker rentAddEditRentDate;
    public DatePicker rentAddEditReturnDate;

    private ViewManager viewManager;
    private RentService rentService;
    private EmployeeService employeeService;
    private CustomerService customerService;
    private CarService carService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        rentService = RentService.getInstance();
        employeeService = EmployeeService.getInstance();
        customerService = CustomerService.getInstance();
        carService = CarService.getInstance();
        viewManager = ViewManager.getInstance();

        if (isEditMode()) {
            loadRentData(State.currentRent);
        }

        try {
            rentAddEditCustomer.getItems().clear();
            rentAddEditCustomer.getItems().addAll(customerService.getAll());

            rentAddEditCar.getItems().clear();
            rentAddEditCar.getItems().addAll(carService.getAll());
        } catch (SQLException throwables) {
            SimpleDialogs.error("Błąd podczas wczytywania listy samochodów i klientów");
            throwables.printStackTrace();
        }
    }

    private void loadRentData(Rent rent) {
        rentAddEditCar.getSelectionModel().select(rent.getCar());
        rentAddEditCustomer.getSelectionModel().select(rent.getCustomer());
        rentAddEditReturnDate.setValue(rent.getReturnDate());
        rentAddEditRentDate.setValue(rent.getRentDate());
    }

    public void rentAddEditLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentAddEditBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("rent/rents.fxml");
    }

    public void rentAddEditSaveClicked(ActionEvent actionEvent) {
        Rent rent = new Rent();

        final Car car = rentAddEditCar.getSelectionModel().getSelectedItem();
        final Customer customer = rentAddEditCustomer.getSelectionModel().getSelectedItem();

        rent.setCar(car);
        rent.setCustomer(customer);
        rent.setRentDate(rentAddEditRentDate.getValue());
        rent.setReturnDate(rentAddEditReturnDate.getValue());
        rent.setCarId(car.getId());
        rent.setCustomerId(customer.getId());

        if (isEditMode()) {
            try {
                rentService.edit(rent, State.currentRent.getId());
            } catch (SQLException throwables) {
                SimpleDialogs.error("Błąd przy edycji wypożyczenia");
                throwables.printStackTrace();
            }
        } else {
            try {
                rentService.insert(rent);
            } catch (SQLException | ClassNotFoundException throwables) {
                SimpleDialogs.error("Błąd przy dodawaniu wypożyczenia");
                throwables.printStackTrace();
            }
        }

        viewManager.switchToView("rent/rents.fxml");
    }

    private boolean isEditMode() {
        return State.currentRent != null;
    }
}
