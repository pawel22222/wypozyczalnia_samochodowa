package pl.rentacar.controller.rental;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import pl.rentacar.State;
import pl.rentacar.model.Rental;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.service.RentalService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.ResourceBundle;

public class RentalController implements Initializable {

    public ListView<Rental> rentalList;
    private ViewManager viewManager;
    private RentalService rentalService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        rentalService = RentalService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        refreshRentalList();
    }

    private void refreshRentalList() {
        try {
            final List<Rental> rentals = rentalService.getAll();
            rentalList.getItems().clear();
            rentalList.getItems().addAll(rentals);
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy wczytywaniu listy wypożyczalni");
            throwables.printStackTrace();
        }
    }

    public void rentalLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentalBackClicked(ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }

    public void rentalDeleteClicked(ActionEvent actionEvent) {
        final Rental selectedRental = getSelectedRental();
        if (selectedRental == null) return;

        try {
            rentalService.delete(selectedRental.getId());
        } catch (SQLIntegrityConstraintViolationException e) {
            SimpleDialogs.warning("Nie można usunąć wypożyczalni, ponieważ istnieją konta z nią związane");
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu wypożyczalni");
            throwables.printStackTrace();
        }
        refreshRentalList();
    }

    public void rentalAddClicked(ActionEvent actionEvent) {
        State.currentRental = null;
        viewManager.switchToView("rental/rentalAddEdit.fxml");
    }

    public void rentalEditClicked(ActionEvent actionEvent) {
        final Rental selectedRental = getSelectedRental();
        if (selectedRental == null) return;

        State.currentRental = selectedRental;

        viewManager.switchToView("rental/rentalAddEdit.fxml");
    }

    public void rentalViewClicked(ActionEvent actionEvent) {
        final Rental selectedRental = getSelectedRental();
        if (selectedRental == null) return;

        State.currentRental = selectedRental;

        viewManager.switchToView("rental/rentalView.fxml");
    }

    private Rental getSelectedRental() {
        final Rental selectedRental = rentalList.getSelectionModel().getSelectedItems().get(0);

        if (selectedRental == null) {
            SimpleDialogs.warning("Klient nie jest zaznaczony");
            return null;
        }
        return selectedRental;
    }
}
