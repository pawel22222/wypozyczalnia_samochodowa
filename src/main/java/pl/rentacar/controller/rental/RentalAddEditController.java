package pl.rentacar.controller.rental;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.rentacar.State;
import pl.rentacar.model.Rental;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.service.RentalService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class RentalAddEditController implements Initializable {

    public TextField rentalAddEditName;
    public TextField rentalAddEditCity;
    public TextArea rentalAddEditAddress;
    private ViewManager viewManager;
    private RentalService rentalService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        rentalService = RentalService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        if (isEditMode()) {
            loadRentalData(State.currentRental);
        }
    }

    private void loadRentalData(Rental rental) {
        rentalAddEditAddress.setText(rental.getAddress());
        rentalAddEditName.setText(rental.getName());
        rentalAddEditCity.setText(rental.getCity());
    }

    public void rentalAddEditLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentalAddEditBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("rental/rentals.fxml");
    }

    public void rentalAddEditSaveClicked(ActionEvent actionEvent) {
        Rental rental = new Rental();

        rental.setAddress(rentalAddEditAddress.getText());
        rental.setName(rentalAddEditName.getText());
        rental.setCity(rentalAddEditCity.getText());

        if (isEditMode()) {
            try {
                rentalService.edit(rental, State.currentRental.getId());
            } catch (SQLException throwables) {
                SimpleDialogs.error("Błąd przy edycji wypożyczalni");
                throwables.printStackTrace();
            }
        } else {
            try {
                rentalService.insert(rental);
            } catch (SQLException | ClassNotFoundException throwables) {
                SimpleDialogs.error("Błąd przy dodawaniu wypożyczalni");
                throwables.printStackTrace();
            }
        }

        viewManager.switchToView("rental/rentals.fxml");
    }

    private boolean isEditMode() {
        return State.currentRental != null;
    }
}
