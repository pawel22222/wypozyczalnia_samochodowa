package pl.rentacar.controller.rental;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import pl.rentacar.State;
import pl.rentacar.model.Rental;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.util.ResourceBundle;

public class RentalViewController implements Initializable {

    public Label rentalViewName;
    public Label rentalViewCity;
    public Label rentalViewAddress;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        Rental rental = State.currentRental;

        if (rental == null) {
            SimpleDialogs.error("Nie wczytano wypożyczalni");
            return;
        }

        rentalViewName.setText(rental.getName());
        rentalViewCity.setText(rental.getCity());
        rentalViewAddress.setText(rental.getAddress());
    }

    public void rentalViewLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void rentalViewBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("rental/rentals.fxml");
    }
}
