package pl.rentacar.controller.employee;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminController implements Initializable {
    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();
    }

    public void adminLogoutClicked(final ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void adminCustomersClicked(final ActionEvent actionEvent) {
        viewManager.switchToView("customer/customers.fxml");
    }

    public void adminCarsClicked(final ActionEvent actionEvent) {
        viewManager.switchToView("car/cars.fxml");
    }

    public void adminEmployeesClicked(final ActionEvent actionEvent) {
        viewManager.switchToView("employee/employees.fxml");
    }

    public void adminRentsClicked(final ActionEvent actionEvent) {
        viewManager.switchToView("rent/rents.fxml");
    }

}
