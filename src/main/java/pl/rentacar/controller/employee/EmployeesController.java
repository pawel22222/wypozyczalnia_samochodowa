package pl.rentacar.controller.employee;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import pl.rentacar.model.Employee;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class EmployeesController implements Initializable {
    public ListView<Employee> employeesList;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        refreshEmployeeList();
    }

    private void refreshEmployeeList() {
        try {
            java.util.List<Employee> employeeList = employeeService.getAll();
            employeeList = employeeList.stream()
                    .filter(employee -> !employee.isSuperAdmin()).collect(Collectors.toList());

            employeesList.getItems().clear();
            employeesList.getItems().addAll(employeeList);
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy wczytywaniu listy użytkowników");
            throwables.printStackTrace();
        }
    }

    public void employeesLogoutClicked(final ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void employeesBackClicked(final ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }

    public void employeesDeleteClicked(final ActionEvent actionEvent) {
        final Employee selectedEmployee = employeesList.getSelectionModel().getSelectedItems().get(0);

        if (selectedEmployee.getLogin().equals(employeeService.getLoggedInUser().getLogin())) {
            SimpleDialogs.error("Nie można usunąć swojego konta");
            return;
        }

        try {
            employeeService.delete(selectedEmployee.getId());
            refreshEmployeeList();
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu pracownika");
            throwables.printStackTrace();
        }
    }

    public void employeesPromoteClicked(final ActionEvent actionEvent) {
        final Employee selectedEmployee = employeesList.getSelectionModel().getSelectedItems().get(0);

        if (selectedEmployee.isAdmin()) {
            SimpleDialogs.warning("Pracownik jest już administratorem");
            return;
        }

        try {
            employeeService.setAdmin(selectedEmployee.getId(), true);
            refreshEmployeeList();
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu pracownika");
            throwables.printStackTrace();
        }
    }

    public void employeesDegradeClicked(final ActionEvent actionEvent) {
        final Employee selectedEmployee = employeesList.getSelectionModel().getSelectedItems().get(0);

        if (selectedEmployee.getLogin().equals(employeeService.getLoggedInUser().getLogin())) {
            SimpleDialogs.error("Nie można degradować swojego konta");
            return;
        }

        if (!selectedEmployee.isAdmin()) {
            SimpleDialogs.warning("Pracownik jest już zwykłym użytkownikiem");
            return;
        }

        try {
            employeeService.setAdmin(selectedEmployee.getId(), false);
            refreshEmployeeList();
        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu pracownika");
            throwables.printStackTrace();
        }
    }
}
