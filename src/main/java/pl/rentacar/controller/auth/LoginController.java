package pl.rentacar.controller.auth;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    public TextField loginUsername;
    public PasswordField loginPassword;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();
    }

    public void loginLoginClicked(final ActionEvent actionEvent) {
        final String username = loginUsername.getText();
        final String password = loginPassword.getText();

        try {
            final boolean isUser = employeeService.isThere(username, password);

            if (isUser) {
                employeeService.setLoggedInUser(employeeService.getByUsername(username));
                employeeService.redirectByRole(viewManager);
            } else {
                SimpleDialogs.warning("Niepoprawny login lub hasło");
                employeeService.logout();
            }

        } catch (SQLException | ClassNotFoundException throwables) {
            SimpleDialogs.error("Nie można zalogować");
            throwables.printStackTrace();
            System.exit(1);
        }
    }

    public void loginRegisterClicked(final ActionEvent actionEvent) {
        viewManager.switchToView("auth/register.fxml");
    }

    public void loginCancelClicked(final ActionEvent actionEvent) {
        System.exit(0);
    }
}
