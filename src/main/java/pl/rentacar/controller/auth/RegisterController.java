package pl.rentacar.controller.auth;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import pl.rentacar.model.Employee;
import pl.rentacar.model.Rental;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.service.RentalService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class RegisterController implements Initializable {
    public TextField registerUsername;
    public PasswordField registerPassword;
    public ComboBox<String> registerRental;
    public TextField registerName;
    public TextField registerSurname;

    RentalService rentalService;
    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        rentalService = RentalService.getInstance();

        List<String> rentalNames;
        try {
            rentalNames = rentalService.getAll().stream()
                    .map(rental -> rental.getName() + ", " + rental.getCity())
                    .collect(Collectors.toList());

            registerRental.getItems().addAll(rentalNames);
            registerRental.getSelectionModel().select(0);

        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();
    }

    public void registerRegisterClicked(final ActionEvent actionEvent) {
        final String password = registerPassword.getText();
        final String username = registerUsername.getText();
        final String name = this.registerName.getText();
        final String surname = this.registerSurname.getText();

        String selectedRental = registerRental.getSelectionModel().getSelectedItem();
        String[] split = selectedRental.split(",");
        String rentalName = split[0];

        Rental rental;
        try {
            rental = rentalService.getByName(rentalName);
            Integer rentalId = rental.getId();

            final Employee newEmployee = new Employee(rentalId, name, surname, username, password, "user");

            try {
                employeeService.insert(newEmployee);
                viewManager.switchToView("auth/login.fxml");
            } catch (SQLException | ClassNotFoundException throwables) {
                SimpleDialogs.error("Nie można dodać użytkownika");
                throwables.printStackTrace();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void registerCancelClicked(final ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }
}
