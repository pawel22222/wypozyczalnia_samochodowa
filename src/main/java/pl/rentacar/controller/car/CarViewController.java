package pl.rentacar.controller.car;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import pl.rentacar.State;
import pl.rentacar.model.Car;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.util.ResourceBundle;

public class CarViewController implements Initializable {

    public Label carViewMake;
    public Label carViewModel;
    public Label carViewFuel;
    public Label carViewMileage;
    public Label carViewYear;
    public Label carViewPrice;
    public Label carViewAvailability;
    public Label carViewBorrowed;

    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        Car c = State.currentCar;

        if (c == null) {
            SimpleDialogs.error("Nie wczytano samochodu");
            return;
        }

        carViewMake.setText(c.getMake());
        carViewModel.setText(c.getModel());
        carViewFuel.setText(c.getFuel());
        carViewMileage.setText(String.valueOf(c.getMileage()));
        carViewYear.setText(String.valueOf(c.getYear()));
        carViewPrice.setText(String.valueOf(c.getPrice()));
        carViewAvailability.setText(c.getAvailability());
        carViewBorrowed.setText(c.getBorrowed() ? "TAK" : "NIE");
    }

    public void carViewLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void carViewBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("car/cars.fxml");
    }
}
