package pl.rentacar.controller.car;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import pl.rentacar.State;
import pl.rentacar.model.Car;
import pl.rentacar.model.Customer;
import pl.rentacar.service.CarService;
import pl.rentacar.service.CustomerService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public class CarsController implements Initializable {

    public ListView<Car> carsList;

    private CarService carService;
    private EmployeeService employeeService;
    private ViewManager viewManager;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        carService = CarService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        refreshCarList();
    }

    private void refreshCarList() {
        try {
            final List<Car> cars = carService.getAll();
            carsList.getItems().clear();
            carsList.getItems().addAll(cars);
        } catch (SQLException throwables) {
            SimpleDialogs.error("Błąd przy wczytywaniu listy samochodów");
            throwables.printStackTrace();
        }
    }

    public void carsLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void carsBackClicked(ActionEvent actionEvent) {
        employeeService.redirectByRole(viewManager);
    }

    public void carsDeleteClicked(ActionEvent actionEvent) {
        final Car selectedCar = carsList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCar == null) {
            SimpleDialogs.warning("Samochód nie jest zaznaczony");
            return;
        }

        try {
            carService.delete(selectedCar.getId());
            refreshCarList();
        } catch (SQLException throwables) {
            SimpleDialogs.error("Błąd przy usuwaniu samochodu");
            throwables.printStackTrace();
        }
    }

    public void carsAddClicked(ActionEvent actionEvent) {
        State.currentCar = null;
        viewManager.switchToView("car/carAddEdit.fxml");
    }

    public void carsEditClicked(ActionEvent actionEvent) {
        final Car selectedCar = carsList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCar == null) {
            SimpleDialogs.warning("Samochód nie jest zaznaczony");
            return;
        }

        State.currentCar = selectedCar;

        viewManager.switchToView("car/carAddEdit.fxml");
    }

    public void carsViewClicked(ActionEvent actionEvent) {
        final Car selectedCar = carsList.getSelectionModel().getSelectedItems().get(0);

        if (selectedCar == null) {
            SimpleDialogs.warning("Samochód nie jest zaznaczony");
            return;
        }

        State.currentCar = selectedCar;

        viewManager.switchToView("car/carView.fxml");
    }
}
