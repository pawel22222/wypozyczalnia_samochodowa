package pl.rentacar.controller.car;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import pl.rentacar.State;
import pl.rentacar.model.Car;
import pl.rentacar.model.Employee;
import pl.rentacar.service.CarService;
import pl.rentacar.service.EmployeeService;
import pl.rentacar.view.SimpleDialogs;
import pl.rentacar.view.ViewManager;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class CarAddEditController implements Initializable {

    public TextField carAddEditMake;
    public TextField carAddEditModel;
    public TextField carAddEditYear;
    public ComboBox<String> carAddEditFuel;
    public TextField carAddEditMileage;
    public TextField carAddEditPrice;
    public TextField carAddEditAvailability;

    private ViewManager viewManager;
    private CarService carService;
    private EmployeeService employeeService;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        carService = CarService.getInstance();
        employeeService = EmployeeService.getInstance();
        viewManager = ViewManager.getInstance();

        carAddEditFuel.getItems().addAll("benzyna", "benzyna + LPG", "benzyna + CNG", "diesel",
                "elektryczny", "hybryda");

        if (isEditMode()) {
            loadCarData(State.currentCar);
        }
    }

    private void loadCarData(Car c) {
        carAddEditMake.setText(c.getMake());
        carAddEditModel.setText(c.getModel());
        carAddEditFuel.getSelectionModel().select(c.getFuel());
        carAddEditMileage.setText(String.valueOf(c.getMileage()));
        carAddEditYear.setText(String.valueOf(c.getYear()));
        carAddEditPrice.setText(String.valueOf(c.getPrice()));
        carAddEditAvailability.setText(c.getAvailability());
    }

    public void carAddEditLogoutClicked(ActionEvent actionEvent) {
        employeeService.logout();
    }

    public void carAddEditBackClicked(ActionEvent actionEvent) {
        viewManager.switchToView("car/cars.fxml");
    }

    public void carAddEditSaveClicked(ActionEvent actionEvent) {
        Car car = new Car();

        final Employee loggedInUser = employeeService.getLoggedInUser();
        car.setRentalId(loggedInUser.getRentalId());

        car.setMake(carAddEditMake.getText());
        car.setModel(carAddEditModel.getText());
        car.setFuel(carAddEditFuel.getSelectionModel().getSelectedItem());
        car.setMileage(Integer.parseInt(carAddEditMileage.getText()));
        car.setYear(Integer.parseInt(carAddEditYear.getText()));
        car.setPrice(Double.valueOf(carAddEditPrice.getText()));
        car.setAvailability(carAddEditAvailability.getText());

        if(State.currentCar != null) {
            car.setBorrowed(State.currentCar.getBorrowed());
        } else {
            car.setBorrowed(false);
        }

        if (isEditMode()) {
            try {
                carService.edit(car, State.currentCar.getId());
            } catch (SQLException throwables) {
                SimpleDialogs.error("Błąd przy edycji samochodu");
                throwables.printStackTrace();
            }
        } else {
            try {
                carService.insert(car);
            } catch (SQLException | ClassNotFoundException throwables) {
                SimpleDialogs.error("Błąd przy dodawaniu samochodu");
                throwables.printStackTrace();
            }
        }

        viewManager.switchToView("car/cars.fxml");
    }

    private boolean isEditMode() {
        return State.currentCar != null;
    }
}
