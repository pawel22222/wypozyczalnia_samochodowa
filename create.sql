DROP DATABASE IF EXISTS `rentacar`;
CREATE DATABASE `rentacar`;
USE `rentacar`;

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `surname` varchar(128) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
);

DROP TABLE IF EXISTS `rental`;
CREATE TABLE `rental` (
  `rental_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `address` varchar(256) NOT NULL,
  `city` varchar(64) NOT NULL,
  PRIMARY KEY (`rental_id`)
);

DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `car_id` int NOT NULL AUTO_INCREMENT,
  `rental_id` int NOT NULL,
  `make` varchar(64) NOT NULL,
  `model` varchar(128) NOT NULL,
  `year` date DEFAULT NULL,
  `fuel` varchar(32) DEFAULT NULL,
  `mileage` int DEFAULT NULL,
  `price` varchar(32) NOT NULL,
  `availability` varchar(32) NOT NULL,
  PRIMARY KEY (`car_id`),
  KEY `car_rental_idx` (`rental_id`),
  CONSTRAINT `car_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`)
);

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `employee_id` int NOT NULL AUTO_INCREMENT,
  `rental_id` int NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `login` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `is_admin` tinyint DEFAULT '0',
  PRIMARY KEY (`employee_id`),
  KEY `employee_rental_idx` (`rental_id`),
  CONSTRAINT `employee_rental` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`rental_id`)
);

DROP TABLE IF EXISTS `rent`;
CREATE TABLE `rent` (
  `rent_id` int NOT NULL AUTO_INCREMENT,
  `car_id` int NOT NULL,
  `rent_date` date NOT NULL,
  `return_date` date NOT NULL,
  `customer_id` int NOT NULL,
  PRIMARY KEY (`rent_id`),
  KEY `rent_car_idx` (`car_id`),
  KEY `rent_customer_idx` (`customer_id`),
  CONSTRAINT `rent_car` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  CONSTRAINT `rent_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`)
);
